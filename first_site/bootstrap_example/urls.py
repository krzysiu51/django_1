from django.urls import path

from . import views

urlpatterns = [
    path('', views.bindex_view, name='bindex'),
    path('bindex/', views.bindex_view, name='bindex'),
]